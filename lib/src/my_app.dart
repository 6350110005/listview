import "package:flutter/material.dart";
import 'package:listview2/src/page/example1.dart';
import 'package:listview2/src/page/example2.dart';
import 'package:listview2/src/page/example3.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Listview Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Example3(),
    );
  }
}