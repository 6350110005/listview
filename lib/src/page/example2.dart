import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listview2'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
              Icons.directions_railway,
              size: 45,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_railway,
              size: 45,
            ),
            title: Text(
              '8.20 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_railway,
              size: 45,
            ),
            title: Text(
              '8.40 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 45,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 45,
            ),
            title: Text(
              '8.15 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 45,
            ),
            title: Text(
              '8.30 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_bus,
              size: 45,
            ),
            title: Text(
              '7.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_bus,
              size: 45,
            ),
            title: Text(
              '7.15 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_ferry,
              size: 45,
            ),
            title: Text(
              '7.45 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_car,
              size: 45,
            ),
            title: Text(
              '8.30 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_car,
              size: 45,
            ),
            title: Text(
              '9.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_car,
              size: 45,
            ),
            title: Text(
              '9.30 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),

          ListTile(
            leading: Icon(
              Icons.directions_car,
              size: 45,
            ),
            title: Text(
              '10.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello world Sunny.',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 45,
            ),
            onTap: (){
              print("Train");
            },
          ),
        ],
      ),
    );
  }
}