import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
  Example3({Key? key}) : super(key: key);

  final titles = ['Brownies', 'Ice-cream sandwic', 'Browned butter caramel',
    'Cake', 'Apple pie', 'Cookie', 'Cupcake', 'Pancakes', 'Pudding'];

  final imgs = ["lib/assets/images/borny.jpeg",
    "lib/assets/images/icecreamsandwic.jpg",
    "lib/assets/images/Browned Butter Caramel.jpg",
    "lib/assets/images/cake.jpg",
    "lib/assets/images/classic-apple-pie.jpg",
    "lib/assets/images/cookie.jpg",
    "lib/assets/images/cupcake.jpg",
    "lib/assets/images/pancakes.jpg",
    "lib/assets/images/pudding.jpg",];

  final subtitles = ['from london','from los angeles','from sydney',
    'from tokyo','from new york','from bangkok',
    'from osaka','from berlin','from paris'];

  //final img = [Image(image: image),Image(image: image),Image(image: image),];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listview3'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index){
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('${imgs[index]}'),
                  radius: 35,
                ),
                title: Text('${titles[index]}',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                subtitle: Text('${subtitles[index]}', style: TextStyle(fontSize: 18),),
                trailing: Icon(Icons.star_border_rounded,size: 35,),
              ),
              Divider(thickness: 1,)
            ],
          );
        },

      ),
    );
  }
}
